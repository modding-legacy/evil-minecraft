![Evil Minecraft](https://moddinglegacy.com/assets/images/mods/banner/evil-minecraft.png)

# What is Evil Minecraft?
Evil Minecraft is a mod in development that will eventually be a total conversion of Minecraft into a completely new experience for players. We hope to add many features, most of which we aren't going to be announcing quite yet.

# Original Credits
### Project Director
* Miclee

### Programmers
* Shadow386(lead)
* TheDarkKnight
* Fligabob
* Flexicode
* Pepper
* Pwootage
* SmallDeadGuy
* Tundmatu
* WackoMcGoose

### Texturers
* RicksterEpicWin
* QSKSw

### Testers
* clarjon1
* xxxzzzmark
* GLaDOS / Space_Core

### Special Thanks
* Vahkiti
* Cameron_D
* Risugami
* Hippoplatimus
* l4mRh4X0r
* Corosus
* XaPhobia / X__x
* billkorbecki
* Pchan3
* UltraMoogleMan
* clone1018
